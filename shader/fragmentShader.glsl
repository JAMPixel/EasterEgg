#version 330 core

uniform sampler2D image;

out vec4 color;
in vec3 color_out;


void main()
{
    color = texture2D(image, vec2(color_out.x,color_out.y));
    //color = vec4(color_out, 0.5);
}