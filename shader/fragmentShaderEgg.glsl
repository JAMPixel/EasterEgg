#version 330 core

uniform sampler2D imageEgg;

out vec4 color;
in vec3 color_out;


void main()
{
    color = texture2D(imageEgg, vec2(color_out.x,color_out.y));
}