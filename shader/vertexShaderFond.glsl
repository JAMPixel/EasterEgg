#version 330 core

layout(location = 0)in vec3 claude;

uniform vec3 posLapin;

out vec3 color_out;

void main()
{
    color_out = claude;
    gl_Position = vec4(claude, 1.);
}