#version 330 core

layout(location = 0)in vec3 claude;

uniform vec3 posLapin;

out vec3 color_out;

void main()
{
    color_out = -1* (claude+vec3(0.5,0.5,0.5));
    gl_Position = vec4((0.2*claude+posLapin), 1.);
}
