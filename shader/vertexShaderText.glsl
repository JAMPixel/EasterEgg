#version 330 core

layout(location = 0)in vec3 claude;
uniform int nbCarac;
uniform int largeur;
uniform int hauteur;
uniform vec2 translation;

out vec3 color_out;

void main()
{
    vec3 uv;
    uv.x = (claude.x+1.f)/2.f;
    uv.y = (claude.y+1.f)/(-2.f);
    color_out = uv;
    gl_Position = vec4((mat3(nbCarac*80.f/float(largeur),0.,0.,0.,80.f/float(hauteur),0.,0.,0.,1.)*claude)+vec3(translation,0.), 1.);
}
