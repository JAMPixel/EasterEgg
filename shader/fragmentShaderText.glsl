#version 330 core

uniform sampler2D imageTexte;

out vec4 color;
in vec3 color_out;


void main()
{
    vec4 texel = texture2D(imageTexte, color_out.xy);

    color = mix(vec4(0.,0.,0.,0.2),vec4(texel.rgb, 1.0), texel.a);
}