#version 330 core
#define F(x) (log(x))
out vec4 color;
in vec3 color_out;
uniform float t;
uniform vec2 coordBalle;
uniform int nbSpire;

const vec3 colors[3]=vec3[](
    vec3(1.0,0.75,0.0),
    vec3(0.5, 0.75, 0.5),
    vec3(0.75, 0.5, 0.25)
    /*vec3(1.0,0.,0.),
    vec3(0.0,1.,0.),
    vec3(0.0,0.,1.)*/
);
const float pi=3.14159;

void main()
{
    vec3 uv = color_out;
    //vec3 uv = (color_out+1)/2;
    float x = uv.x;
    float y = uv.y;


    float angle = atan(y-coordBalle.y,x-coordBalle.x);


    float norm = length(vec2(x-coordBalle.x,y-coordBalle.y));

    float a = (nbSpire/10.f)*sqrt(norm)+((int(norm*10)%2 == 0) ? 1.0f : -1.0f)*angle-0.7f*t;
    float val = sin(a);
    float alpha = 1.0f;

    if(int(a/pi+1/2.0f)%2==0)
    {
        if(val<-0.9 || (val > -0.7 && val < -0.6) || (val > -0.4 && val < -0.3) || (val > 0.1 && val < 0.0) || (val > 0.2 && val < 0.3) || (val > 0.5 && val < 0.6) || (val > 0.8 && val < 0.9))
            {
                color = vec4(colors[0],alpha);
            }
            else
            {
                if(val<-0.8 || (val > -0.6 && val < -0.5) || (val > -0.3 && val < -0.2) || (val > 0.0 && val < 0.1) || (val > 0.3 && val < 0.4) || (val > 0.6 && val < 0.7) || (val > 0.9 && val < 10.0))
                {
                    color = vec4(colors[1],alpha);
                }
                else
                {
                    color = vec4(colors[2],alpha);
                }
            }
    }
    else
    {
        if(val<-0.9 || (val > -0.7 && val < -0.6) || (val > -0.4 && val < -0.3) || (val > 0.1 && val < 0.0) || (val > 0.2 && val < 0.3) || (val > 0.5 && val < 0.6) || (val > 0.8 && val < 0.9))
                    {
                        color = vec4(colors[1],alpha);
                    }
                    else
                    {
                        if(val<-0.8 || (val > -0.6 && val < -0.5) || (val > -0.3 && val < -0.2) || (val > 0.0 && val < 0.1) || (val > 0.3 && val < 0.4) || (val > 0.6 && val < 0.7) || (val > 0.9 && val < 10.0))
                        {
                            color = vec4(colors[2],alpha);
                        }
                        else
                        {
                            color = vec4(colors[0],alpha);
                        }
                    }
    }
}