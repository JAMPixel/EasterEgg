EasterEgg
===

## Introduction

EasterEgg est un jeux de pong développé à l'occasion de la JAM Pixel de Pâque 2016

## Compilation

Utiliser le fichier CMakeList.txt pour compiler le jeux avec CMake

## Utilisation

Lorsque le jeux se lance vous démarrez sur le menu qui est à peine commencé. 
Tapé sur ECHAP pour lancer le jeux en lui même.
Le joueur de gauche utilise les comande Z (monter) et D (descendre) et celui de 
droite utilise les flèche haut et bas.

## Technos

* C++11
* SDL2

> sudo apt-get install libsdl2-dev

* SDL2_Image

> sudo apt-get install libsdl2-image-dev

* SDL2_ttf

> sudo apt-get install libsdl2-ttf-dev

* OpenGL3.3

> sudo apt-get install freeglut3 freeglut3-dev
> sudo apt-get install libglew-dev
> sudo apt-get install libglu1-mesa libglu1-mesa-dev
> sudo apt-get install libglm-dev


## Attention 

Le fond du jeux est une spirale avec des couleurs vive en rotation qui peuvent avoir tendance à aggresser les yeux ;)