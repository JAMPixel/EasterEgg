//
// Created by ju on 19/03/16.
//

#ifndef EASTEREGGS_SPRITE_H
#define EASTEREGGS_SPRITE_H

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <cassert>
#include "erreur.hpp"
#include "Program.hpp"

class Sprite {
public:
    Sprite(char const * vertexShader, char const* fragmentShader, char const* image, char const* uni_image);
    GLuint getProgram();
    void beforePrint();
    void print();
private:
    Program prog_;
    GLuint vao_;
    GLint uniform_;
    GLuint texture_;
};


#endif //EASTEREGGS_SPRITE_H
