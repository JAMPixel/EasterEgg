//
// Created by ju on 23/03/16.
//

#ifndef EASTEREGGS_TEXT_GL_H
#define EASTEREGGS_TEXT_GL_H

#include "Program.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

typedef std::pair<GLint,std::string> Uniform;

class Text_gl {
public:
    Text_gl(char const* vertexShader, char const* fragmentShader, char const* police, char const* , SDL_Color couleur,
            int winH, int winW, glm::vec2 translation);
    ~Text_gl();
    GLuint getProgram();
    void useProgram();
    GLuint getVao();
    void beforePrint();
    void print();
    void addUniform(GLint uni_new, std::string nomVar);
    void setPolice(char const* newPolice);
    void setColor(Uint8 r, Uint8 g, Uint8 b);
    void setColor(SDL_Color color);
private:
    Program prog_;
    GLuint vao_;
    GLuint vbo_;
    GLuint texture_;
    SDL_Surface * texte_;
    TTF_Font * police_;
    SDL_Color couleur_;
    int nbCarac_;
    int hauteur_;
    int largeur_;
    std::vector<Uniform> uniformes_;
    std::string chaineTexte_;
    glm::vec2 translation_;
};


#endif //EASTEREGGS_TEXT_GL_H
