//
// Created by ju on 23/03/16.
//

#include "Bouton.h"

Bouton::Bouton(std::string texte, int winH, int winW) : texte_("vertexShaderText.glsl", "fragmentShaderText.glsl", "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", texte.c_str(), {20,45,90}, winH, winW, glm::vec2(0.,0.))
{

}

void Bouton::drawButton()
{
    texte_.beforePrint();
    texte_.print();
}