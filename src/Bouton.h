//
// Created by ju on 23/03/16.
//

#ifndef EASTEREGGS_BOUTON_H
#define EASTEREGGS_BOUTON_H

#include "Text_gl.h"

class Bouton {
public:
    Bouton(std::string texte, int winH, int winW);
    void drawButton();
private:
    Text_gl texte_;
    glm::vec2 coord_;
    glm::vec2 diagonale;

};


#endif //EASTEREGGS_BOUTON_H
