//
// Created by ju on 19/03/16.
//

#include "Sprite.h"

Sprite::Sprite(char const* vertexShader, char const* fragmentShader, char const* image, char const* uni_image) : prog_(vertexShader, fragmentShader)
{
    glUseProgram(prog_.getProgram());

    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);

    GLuint vbo;
    glGenBuffers(1, &vbo);//1 pour un seul vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    GLfloat  sommetCarre[] = {
            -0.5,0.5,0.5,
            0.5,0.5,0.5,
            0.5,-0.5,0.5,
            -0.5,-0.5,0.5,
    };

    GLuint elementBuffer[]={
            0, 1, 2,
            0, 2, 3,
    };

    GLuint eltBuf;
    glGenBuffers(1, &eltBuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBuf);


    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementBuffer), elementBuffer, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sommetCarre), sommetCarre, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(0);

    //texture
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &texture_);
    glBindTexture(GL_TEXTURE_2D, texture_);

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    SDL_Surface * surfaceImage = IMG_Load(image);

    // Format de l'image

    GLenum formatInterne(0);
    GLenum format(0);


    // Détermination du format et du format interne

    if((int)surfaceImage->format->BytesPerPixel == 3)
    {
        // Format interne

        formatInterne = GL_RGB;


        // Format

        if(surfaceImage->format->Rmask == 0xff)
            format = GL_RGB;

        else
            format = GL_BGR;
    }


    // Détermination du format et du format interne pour les images à 4 composantes

    else if((int)surfaceImage->format->BytesPerPixel == 4)
    {
        // Format interne

        formatInterne = GL_RGBA;


        // Format

        if(surfaceImage->format->Rmask == 0xff)
            format = GL_RGBA;

        else
            format = GL_BGRA;
    }


    // Dans les autres cas, on arrête le chargement

    else
    {
        SDL_FreeSurface(surfaceImage);

        assert(false);
    }

    glTexImage2D(GL_TEXTURE_2D, 0, formatInterne, surfaceImage->w, surfaceImage->h, 0, format, GL_UNSIGNED_BYTE, surfaceImage->pixels);

    uniform_ = glGetUniformLocation(prog_.getProgram(),uni_image);

    getError();
}

GLuint Sprite::getProgram()
{
    return prog_.getProgram();
}

void Sprite::beforePrint()
{
    glUseProgram(prog_.getProgram());
    glBindVertexArray(vao_);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glUniform1i(uniform_, 0);
}

void Sprite::print()
{
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);
}