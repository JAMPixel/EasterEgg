//
// Created by ju on 20/03/16.
//

#include "deroulement.h"

Deroulement::Deroulement(SDL_Window * win, int width, int height) : win_(win), width_(width), height_(height)
{

}

void Deroulement::menu()
{
    //Fond

    Program progFond("shader/vertexShaderFond.glsl","shader/fragmentShaderFond.glsl");
    glUseProgram(progFond.getProgram());

    GLuint vaoFond;
    glGenVertexArrays(1, &vaoFond);
    glBindVertexArray(vaoFond);

    GLuint vboFond;
    glGenBuffers(1, &vboFond);
    glBindBuffer(GL_ARRAY_BUFFER, vboFond);

    GLfloat  sommetFond[] = {
            -1.0,1.0,0.5,
            1.0,1.0,0.5,
            1.0,-1.0,0.5,
            -1.0,-1.0,0.5,
    };

    GLuint elementBuffer[]={
            0, 1, 2,
            0, 2, 3,
    };

    std::vector<GLuint> elementBufferV = {
            0, 1, 2,
            0, 2, 3,
    };

    GLuint eltBuf;
    glGenBuffers(1, &eltBuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBuf);

    GLuint * tmp = new GLuint[elementBufferV.size()];
    std::copy(elementBufferV.begin(),elementBufferV.end(),tmp);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*elementBufferV.size(), elementBufferV.data(), GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sommetFond), sommetFond, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(0);

    //Titre
    Text_gl titre("shader/vertexShaderText.glsl", "shader/fragmentShaderText.glsl", "ressource/ADFBEasterEgg.ttf", "easteregg", {255,50,255}, height_, width_, glm::vec2(0.,0.8));

    //Lancer le jeux
    Text_gl jouer("shader/vertexShaderText.glsl", "shader/fragmentShaderText.glsl", "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", "Jouer", {255,50,255}, height_, width_, glm::vec2(0.,0.5));

    //bouton
   // Bouton b(std::string("essais"), height_, width_);

    //variables de boucle
    bool fin = false;
    SDL_Event event;
    GLint uni_T = glGetUniformLocation(progFond.getProgram(),"t");//variable "temps"
    GLint uni_CoordMouse = glGetUniformLocation(progFond.getProgram(),"coordBalle");
    GLint uni_NbSpire = glGetUniformLocation(progFond.getProgram(),"nbSpire");
    float t = 0.f;
    float k = 0.f;
    glm::vec2 coordMouse = glm::vec2(0.0f,0.0f);
    int nbSpire = 20;

    while (!fin)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    fin = true;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE:
                            fin = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_MOUSEMOTION:
                    coordMouse.x = ((float)event.motion.x*2.0f/(float)width_)-1;
                    coordMouse.y = (-(float)event.motion.y*2.0f/(float)height_)+1;

                    break;
                case SDL_MOUSEWHEEL:
                    nbSpire = ((event.wheel.y < 0) ? nbSpire+1 : nbSpire-1);
                default:
                    break;
            }
        }

        //fond
        glUseProgram(progFond.getProgram());
        glBindVertexArray(vaoFond);
        k+=0.01f;
        t = 50.f*tan(k/10.f);
        glUniform1f(uni_T, t);
        glUniform2fv(uni_CoordMouse,1,glm::value_ptr(coordMouse));
        glUniform1i(uni_NbSpire, nbSpire);

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);

        //texte

        jouer.beforePrint();
        jouer.print();

        titre.beforePrint();
        titre.print();

        //bouton
        //b.drawButton();

        SDL_GL_SwapWindow(win_);
    }
}

void Deroulement::jeux()
{
    Program progFond("shader/vertexShaderFond.glsl","shader/fragmentShaderFond.glsl");

    glUseProgram(progFond.getProgram());

    GLuint vaoFond;//vertex array object
    glGenVertexArrays(1, &vaoFond);
    glBindVertexArray(vaoFond);

    GLuint vboFond;
    glGenBuffers(1, &vboFond);//1 pour un seul vbo
    glBindBuffer(GL_ARRAY_BUFFER, vboFond);

    GLfloat  sommetCarre[] = {
            -1.0,1.0,0.5,
            1.0,1.0,0.5,
            1.0,-1.0,0.5,
            -1.0,-1.0,0.5,
    };

    GLuint elementBuffer[]={
            0, 1, 2,
            0, 2, 3,
    };

    GLuint eltBuf;
    glGenBuffers(1, &eltBuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBuf);


    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementBuffer), elementBuffer, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sommetCarre), sommetCarre, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(0);


    //lapin du Joueur

    Sprite lapinJoueur("shader/vertexShaderLapin.glsl","shader/fragmentShaderLapin.glsl","ressource/rabbit_256_blanc.png", "imageLapin");
    glm::vec3 raquette1 = glm::vec3(-0.9f, 0.0f, 0.5f);
    GLint uni_PosLapin = glGetUniformLocation(lapinJoueur.getProgram(),"posLapin");
    glUniform3fv(uni_PosLapin, 1, glm::value_ptr(raquette1));

    //lapin du BOT

    Sprite lapinBot("shader/vertexShaderBot.glsl","shader/fragmentShaderBot.glsl","ressource/rabbit_256_blanc.png", "imageBot");
    glm::vec3 raquette2 = glm::vec3(0.9f, 0.0f, 0.5f);
    GLint uni_PosBot = glGetUniformLocation(lapinBot.getProgram(),"posBot");
    glUniform3fv(uni_PosLapin, 1, glm::value_ptr(raquette2));


    //oeuf

    Sprite egg("shader/vertexShaderEgg.glsl", "shader/fragmentShaderEgg.glsl","ressource/egg.png", "imageEgg");
    glm::vec3 balle = glm::vec3(-0.5f, 0.0f, 0.5f);
    GLint uni_PosEgg = glGetUniformLocation(egg.getProgram(),"posEgg");
    glUniform3fv(uni_PosLapin, 1, glm::value_ptr(balle));
    GLint unit_CoordBalle = glGetUniformLocation(progFond.getProgram(), "coordBalle");
    glUniform2fv(unit_CoordBalle, 1, glm::value_ptr(balle));

    //variables de boucle :

    bool fin = false;
    SDL_Event event;
    float posMouse = 0.0f;
    float t = 0.0f;
    float vitesse = 0.01f;
    glm::vec3 dirBalle = glm::vec3(0.01f, 0.01f,0.0f);
    dirBalle = (vitesse/glm::length(dirBalle))*dirBalle;
    glm::vec3 future = balle+dirBalle;
    GLint uni_T = glGetUniformLocation(progFond.getProgram(),"t");
    GLint uni_NbSpire = glGetUniformLocation(progFond.getProgram(),"nbSpire");
    float angle = 0.0f;
    const Uint8 * clavier = SDL_GetKeyboardState(nullptr);

    while(!fin)
    {


        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                    fin = true;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                        case SDLK_ESCAPE:
                            fin = true;
                            break;
                        default:
                            break;
                    }
                default :
                    break;
            }
        }
        if(clavier[SDL_SCANCODE_W])
        {
            raquette1.y = ((raquette1.y+0.025<0.85) ? raquette1.y+0.025 : raquette1.y);
        }
        if(clavier[SDL_SCANCODE_S])
        {
            raquette1.y = ((raquette1.y-0.025>-0.85) ? raquette1.y-0.025 : raquette1.y);
        }
        if(clavier[SDL_SCANCODE_UP])
        {
            raquette2.y = ((raquette2.y+0.025<0.85) ? raquette2.y+0.025 : raquette2.y);
        }
        if(clavier[SDL_SCANCODE_DOWN])
        {
            raquette2.y = ((raquette2.y-0.025>-0.85) ? raquette2.y-0.025 : raquette2.y);
        }
        t+=0.1f;

        future = balle + dirBalle;

        //colision avec le haut et le bas de la zone de jeux
        if(future.y<0.92)
        {
            if(future.y>-0.92)
            {
                balle = future;
            }
            else
            {
                balle = glm::vec3(future.x, -1.84-future.y,future.z);
                angle = 2*acosf(glm::dot(dirBalle,glm::vec3(1.0,0,0))/glm::length(dirBalle));
                dirBalle = glm::mat3(
                        cos(angle), sin(angle), 0,
                        -sin(angle), cos(angle), 0,
                        0, 0, 1.0
                )*dirBalle;
            }
        }
        else
        {
            balle = glm::vec3(future.x, 1.84-future.y,future.z);
            angle = -2*acos(glm::dot(dirBalle,glm::vec3(1.0,0,0))/glm::length(dirBalle));
            dirBalle = glm::mat3(
                    cos(angle), sin(angle), 0,
                    -sin(angle), cos(angle), 0,
                    0, 0, 1.0
            )*dirBalle;
        }

        if(glm::length(raquette2+glm::vec3(0.15,0,0)-future)<0.3 && dirBalle.x>0)
        {
            dirBalle = (raquette2+glm::vec3(0.15,0,0)-future);
            dirBalle = (vitesse/glm::length(dirBalle))*dirBalle;
            vitesse+=0.001f;
            dirBalle*=-1;
        }

        if(glm::length(raquette1-glm::vec3(0.15,0,0)-future)<0.3 && dirBalle.x<0)
        {
            dirBalle = (raquette1-glm::vec3(0.15,0,0)-future);
            dirBalle = (vitesse/glm::length(dirBalle))*dirBalle;
            vitesse+=0.001f;
            dirBalle*=-1;
        }


        if(balle.x < -1 || balle.x>1)
        {
            vitesse = 0.01f;
            dirBalle = glm::vec3(0.01f, 0.01f,0.0f);
            dirBalle = (vitesse/glm::length(dirBalle))*dirBalle;
        }
        glClear(GL_COLOR_BUFFER_BIT);

        //fond
        glUseProgram(progFond.getProgram());
        glBindVertexArray(vaoFond);

        glUniform1fv(uni_T, 1, &t);
        glUniform2fv(unit_CoordBalle, 1, glm::value_ptr(balle));
        glUniform1i(uni_NbSpire, 100);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);

        //lapin
        lapinJoueur.beforePrint();

        glUniform3fv(uni_PosLapin, 1, glm::value_ptr(raquette1));

        lapinJoueur.print();

        //bot

        lapinBot.beforePrint();

        glUniform3fv(uni_PosBot, 1, glm::value_ptr(raquette2));

        lapinBot.print();

        //egg

        egg.beforePrint();

        glUniform3fv(uni_PosEgg, 1, glm::value_ptr(balle));

        egg.print();

        SDL_GL_SwapWindow(win_);
    }
}
