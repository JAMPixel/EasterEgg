//
// Created by ju on 23/03/16.
//

#include "Text_gl.h"

Text_gl::Text_gl(char const* vertexShader, char const* fragmentShader, char const* police, char const* chaineTexte, SDL_Color couleur, int winH, int winW, glm::vec2 translation) : prog_(vertexShader, fragmentShader), couleur_(couleur), hauteur_(winH), largeur_(winW), chaineTexte_(chaineTexte), translation_(translation)
{
    prog_.useProgram();

    //initialisation du vao (Vertex Array Buffer)
    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);

    //initialisation du vbo
    glGenBuffers(1, &vbo_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_);

    //vertices du quad
    GLfloat sommetZoneTexte[] = {
            -1.0f, 1.0f, 0.5f,
            1.0f, 1.0f, 0.5f,
            1.0f, -1.0f, 0.5f,
            -1.0f, -1.0f, 0.5f
    };

    //ordre des vertices
    GLuint elementBufferTexte[]={
            0, 1, 3,
            1, 2, 3
    };


    GLuint eltBufTexte;
    glGenBuffers(1, &eltBufTexte);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBufTexte);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementBufferTexte), elementBufferTexte, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sommetZoneTexte), sommetZoneTexte, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
    glEnableVertexAttribArray(0);

    uniformes_.push_back(Uniform(glGetUniformLocation(prog_.getProgram(), "imageTexte"),std::string("imageTexte")));
    uniformes_.push_back(Uniform(glGetUniformLocation(prog_.getProgram(),"nbCarac"),std::string("nbCarac")));
    uniformes_.push_back(Uniform(glGetUniformLocation(prog_.getProgram(),"largeur"),std::string("largeur")));
    uniformes_.push_back(Uniform(glGetUniformLocation(prog_.getProgram(),"hauteur"),std::string("hauteur")));
    uniformes_.push_back(Uniform(glGetUniformLocation(prog_.getProgram(),"translation"),std::string("translation")));

    if(TTF_Init() == -1)
    {
        std::cerr << "Erreur d'initialisation de TTF_Init : " << TTF_GetError() << std::endl;
        exit(EXIT_FAILURE);
    }

    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &texture_);
    glBindTexture(GL_TEXTURE_2D, texture_);

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //ouverture de la police
    police_ = TTF_OpenFont(police, 200);
    //création du texte
    texte_ = TTF_RenderText_Blended(police_, chaineTexte_.c_str(), couleur_);
    //définition de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texte_->w, texte_->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texte_->pixels);


    nbCarac_ = (int)std::string(chaineTexte).size();
}

Text_gl::~Text_gl()
{
    TTF_CloseFont(police_);
    TTF_Quit();
}

GLuint Text_gl::getProgram()
{
    return prog_.getProgram();
}

void Text_gl::useProgram()
{
    prog_.useProgram();
}

GLuint Text_gl::getVao()
{
    return vao_;
}

void Text_gl::beforePrint()
{
    prog_.useProgram();
    glBindVertexArray(vao_);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glUniform1i(uniformes_[0].first, 0);
    glUniform1i(uniformes_[1].first, nbCarac_);
    glUniform1i(uniformes_[2].first, largeur_);
    glUniform1i(uniformes_[3].first, hauteur_);
    glUniform2fv(uniformes_[4].first, 1, glm::value_ptr(translation_));
}

void Text_gl::print()
{
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);
}

void Text_gl::addUniform(GLint uni_new, std::string nomVar)
{
    uniformes_.push_back(Uniform(uni_new,nomVar));
}

void Text_gl::setPolice(char const *newPolice)
{
    TTF_CloseFont(police_);
    police_ = TTF_OpenFont(newPolice, 200);
    SDL_FreeSurface(texte_);
    texte_ = TTF_RenderText_Blended(police_, chaineTexte_.c_str(), couleur_);
    //définition de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texte_->w, texte_->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texte_->pixels);
}

void Text_gl::setColor(Uint8 r, Uint8 g, Uint8 b)
{
    couleur_ =  {r,g,b};
    SDL_FreeSurface(texte_);
    texte_ = TTF_RenderText_Blended(police_, chaineTexte_.c_str(), couleur_);
    //définition de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texte_->w, texte_->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texte_->pixels);
}

void Text_gl::setColor(SDL_Color color)
{
    couleur_ = color;
    SDL_FreeSurface(texte_);
    texte_ = TTF_RenderText_Blended(police_, chaineTexte_.c_str(), couleur_);
    //définition de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texte_->w, texte_->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, texte_->pixels);
}