//
// Created by ju on 20/03/16.
//

#ifndef EASTEREGGS_DEROULEMENT_H
#define EASTEREGGS_DEROULEMENT_H

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <GL/glew.h>
#include <glm/matrix.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <iterator>
#include <assert.h>
#include <unistd.h>
#include "Program.hpp"
#include "Sprite.h"
#include "erreur.hpp"
#include "Text_gl.h"
#include "Bouton.h"

class Deroulement {
public:
    Deroulement(SDL_Window * win, int width, int height);
    void menu();
    void jeux();

private:
    SDL_Window * win_;
    int width_;
    int height_;
};


#endif //EASTEREGGS_DEROULEMENT_H
