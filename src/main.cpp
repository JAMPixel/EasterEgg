#include <iostream>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <glm/matrix.hpp>
#include "deroulement.h"

using namespace std;

const int HEIGHT = 800;
const int WIDTH = 1200;

int main()
{
	assert(!SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS));

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);//1 seul double buffer

	SDL_DisplayMode d;
	SDL_GetDesktopDisplayMode(0, &d);

	SDL_Window * win = SDL_CreateWindow("glWindow",0,0,d.w,d.h, SDL_WINDOW_SHOWN|SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN_DESKTOP);

	SDL_GLContext context = SDL_GL_CreateContext(win);

	glewExperimental = true;

	assert(!glewInit());
	(void)glGetError();

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

//    // Program
//    Program program("shader/vertexShaderFond.glsl","shader/fragmentShaderFond.glsl");
//    glUseProgram(program.getProgram());
//
//    GLuint vao;
//    glGenVertexArrays(1, &vao);
//    glBindVertexArray(vao);
//
//    GLuint vboFond;
//    glGenBuffers(1, &vboFond);
//    glBindBuffer(GL_ARRAY_BUFFER, vboFond);
//
//    GLfloat  sommetFond[] = {
//            -1.0,1.0,0.5,
//            1.0,1.0,0.5,
//            1.0,-1.0,0.5,
//            -1.0,-1.0,0.5,
//    };
//
////    GLuint elementBuffer[]={
////            0, 1, 2,
////            0, 2, 3,
////    };
//
//    std::vector<GLuint> elementBufferV = {
//            0, 1, 2,
//            0, 2, 3,
//    };
//
//    GLuint eltBuf;
//    glGenBuffers(1, &eltBuf);
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBuf);
//
//    GLuint * tmp = new GLuint[elementBufferV.size()];
//    std::copy(elementBufferV.begin(),elementBufferV.end(),tmp);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*elementBufferV.size(), elementBufferV.data(), GL_STATIC_DRAW);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(sommetFond), sommetFond, GL_STATIC_DRAW);
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
//    glEnableVertexAttribArray(0);
//
//    for(int i=0;i<1000000;++i)
//    {
//        //fond
//        glUseProgram(program.getProgram());
//        glBindVertexArray(vao);
//
//        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);
//        SDL_GL_SwapWindow(win);
//    }
//
//    return EXIT_SUCCESS;

	Deroulement der(win, d.w, d.h);

	der.menu();

	der.jeux();

	return 0;
}
